
# SSO Helpers for C++

This is a standalone library extracted from existing ATLAS TDAQ code to
acquire session cookies or OIDC tokens for the new CERN single sign-on.

It essentially does the same job as https://gitlab.cern.ch/authzsvc/tools/auth-get-sso-cookie
but in pure C++, using only libcurl.

Existing solutions for this are usually calling out to the python scripts
above, but this has several disadvantages:

  * It requires starting a python interpreter in addition to the already
    slow Kerberos authentication (SPNEG).
  * The ATLAS software uses Python from the LCG stack, which uses a
    custom initialization, adding even more than the usual overhead
    (especially if the CVMFS cache is cold).
  * Using the Python scripts from online applications/libraries introduces a
    run-time dependency on Python setup for even the simplest tasks.

## Building and Installing

The top level `CMakeLists.txt` file is for use in the ATLAS Trigger/DAQ
software environment.

To build a stand-alone version of this library (requires libcurl):

```shell
git clone https://gitlab.cern.ch/atlas-tdaq-software/sso-helper.git
mkdir build
cd build
cmake ../sso-helper/standalone
make
```

If you don't have the static libstd++.a installed (it is in a separate
RPM on RedHat), add `-D BUILD_STATIC_LIBSTDCPP=OFF` to the cmake
command.

Add `-D CMAKE_INSTALL_PREFIX=..` to the cmake command to change
to a non-default installation location.

Optionally:

```shell
# on Ubuntu/Debian
cpack -G DEB

# on Fedora/RedHat/CentOS/Rock/Alma/Oracle
cpack -G RPM
```

## Command Line Tools

The `get-sso` command can be used to acquire cookies and web pages via kerberos:

```shell
% ./get-sso -h
Access SSO protected webpage and store cookies

  get-sso -u <url> [-q] [ -o <output-file> ] [ -j <cookie-jar> ]
   -q do not print any output
   -u <url>         the URL to retrieve
   -j <cookie-jar>  specify where to store cookies (default /tmp/sso-cookies-$(id -u)
   -o <output-file> print output into file. Use '-o -' to print to stdout (default)

Show this help
  get-sso -h
```

The `get-sso-token` command can be used as a generic tool to acquire tokens for
public and confidential clients, as well as refresh and exchange tokens.

```shell
% ./get-sso-token -h
Acquire token for client

 get-sso-token krb      -c <client-id> -u <redirect_uri> [ -o <output-file> ] [ -s '<scopes...>' ] [ -j <cookie-jar> ] [-A|-R]
 get-sso-token browser  -c <client-id> -u <redirect_uri> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]
   Note: One of the valid redirection URLs *must* be http://localhost for browser option
 get-sso-token secret   -c <client-id> -p <client-secret> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]
 get-sso-token device   -c <client-id> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]
 get-sso-token password -c <client-id> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]
 get-sso-token auto     -c <client-id> [ -u <redirect_uri> ] [ -o <output-file> ] [ -s '<scopes...>' ] [ -j <cookie-jar> ] [-A|-R]
   Note: tries krb, browser, device in turn
 get-sso-token api      -c <client-id> -p <client-secret> -t <target-client-id> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]

Refresh token

 get-sso-token refresh  -c <client-id> -r <refresh-token>  [ -o <output-file> ] [-A|-R]
   Note: Use '-r -' to read the refresh token from stdin

Exchange token

 get-sso-token exchange -c <client-id> -p <client-secret> -t <target-client -T <access_token> [-A|-R]
   Note: Use '-T -' to read the access token from stdin

 -A Only print access token from result
 -R Only print refresh token from results

 The default cookie jar is /tmp/sso-cookies-$(id -u)

Show this help

 get-sso-token -h
```

## API

### Errors

In case of an error the library throws an exception of (sub-)type
`daq::sso::SSOException` which in turns inherits from `std::exception`.

### Session Cookies

Session cookies are needed to access a SSO protected web page which has
no other authorization method (e.g. via OIDC and access tokens). Note that
the latter is preferred over session cookies. An example is the online
web server https://atlasop.cern.ch .

The function to acquire cookies using Kerberos takes a URL,
and the name of a cookie file. The cookie file name
is optional.

If the file name is empty, cookies are only stored
in memory, which is not of much use, except if you want to download
exactly one URL.

Note: Since acquiring cookies requires to download the relevant page, the function
returns the content of the URL that is given. So an application can use this
to download the first (or only) page without further work.

An application should use a well-defined cookie file and re-use it
among incarnations if possible. Each new session that is created and lives
for a long time is an additional load on the CERN authentication server. On the other
hand re-using an existing sessions speeds up things.

Note: cookie files should be per-user. Either put them into `$XDG_CACHE_DIR` (e.g. $HOME/.cache/myapp-cookies.txt)
or make the path unique (e.g /tmp/myapp-cookies-13331.txt).

```c++

#include "sso-helper/sso-helper.h"

#include <iostream>
#include <memory>

int main()
{
    std::string page = daq::sso::get("https://atlasop.cern.ch", "/tmp/mycookies.txt");
    if (!page.empty()) {

        CURL *handle = curl_easy_init();

        // Access API behind SSO
        curl_easy_setopt(handle, CURLOPT_URL, "https://atlasop.cern.ch/info/current?format=json");
        curl_easy_setopt(handle, CURLOPT_COOKIEJAR, "/tmp/mycookies.txt");
        curl_easy_setopt(handle, CURLOPT_COOKIEFILE, "/tmp/mycookies.txt");

        // my_handler reads returned bytes and puts it into 'result'
        std::string result;
        curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
        curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, my_handler);

        if(!curl_easy_perform(handle)) {
           // success
           std::cout << result << std::endl;
           return 0;
        }
    } // else error
    return 1;
}
```

### OIDC Access Tokens

Access tokens for OIDC applications can be acquired for public clients using
several different methods. The kerberos interface is similar to the one above,
with the client ID taking the place of the URL

```c++

#include "sso-helper/sso-helper.h"
#include <nlohmann/json.hpp>

int main()
{
    std::string result = daq::sso::get_token_from_kerberos("sso-helper-test", "http://localhost", "/tmp/mycookies.txt");

    if(!result.empty()) {
        // parse result
        daq::sso::Tokens tokens;
        daq::sso::parse_tokens(result, tokens);

        CURL *handle = curl_easy_init();
        curl_easy_setopt(handle, CURLOPT_URL, "https://...");
        curl_easy_setopt(handle, CURLOPT_XOAUTH2_BEARER, tokens.access_token.c_str());
        curl_easy_perform(handle);
    }
}
```

If you know your users are in an interactive/graphical environment, you can use their browser
to acquire a token:

```c++
    std::string result = daq::sso::get_token_from_browser("sso-helper-test");
    // rest as above...
```

If you are e.g. logged into a remote host via ssh and no X11 forwarding, but you have
a browser in your local environment available, you can use the device authorization grant:

```c++
    std::string result = daq::sso::get_token_from_device_code("sso-helper-test");
```

If your access token has expired you can get a new one with the refresh token you got in
the first step:

```c++
   std::string result = daq::sso::get_token_from_refresh_token("sso-helper-test", refresh_token);
   // save both "access_token" and "refresh_token" from the result as above
```

Finally you, if you are really desperate, you can ask the user for his/her password. This obviously
violates all good security practices (how do they know what you do with it ?), and they would be right
to not do it...Furthermore this grant type will be no longer available in OAuth 2.1 (but may still
be supported by any existing implementation if they decide so).

This may also require special settings in the authentication server to enable this grant.

```c++
#include <unistd.h>
...
   std::string user = getenv("USER") ? getenv("USER") : "whoareyou";
   std::string password = getpass("Password: ");
   std::string result = daq::sso::get_token_from_password("sso-helper-test", nullptr, user, password);
```

If your server/API is willing to access tokens that are not associated with a user (e.g. from
an application instead) you can use the client credentials grant. Note that the `sub` attribute
will be something like `service-account-my-client-id`. Also you cannot require a default role
for your application where the user must be authenticated, because the aforementioned service
account does not correspond to any real user.

```c++
   std::string result = daq::sso::get_token_from_client_secret("my-client-id", "my-client-secret");
   daq::sso::parse_tokens(result, tokens);
```

You can set up a target application such that it allows to exchange an existing token to one of
the target application (this needs agreement from both sides). In this case you can exchange
an existing access token:

```c++
   std::string result = daq::sso::exchange_token("my-client-id", "my-client-secret", tokens.access_token, "target-client-id");
```

Finally you can use the CERN specific API where you don't need to setup the token exchange. Again,
the token will not be associated with a real user.

```c++
  std::string result = get_token_from_auth_api("my-client-id", "my-client-secret", "target-client-id");
```

## Offline refresh tokens

For long-running non-interactive applications you may want an offline refresh token with
a long lifetime. It should be protected the same way as a password, kerberos ticket, ssh private key, ...
etc.

Most functions take an additional parameter `scope` which is a list of white space separated
OIDC scopes. If you pass `offline_access` as scope, you will get a refresh token that is not
limited to 10 hours. You can store it (protected !) and use at a later time. If it is not
used for 30 days, it becomes invalid, however.


```c++
    auto result = nlohmann::json::parse(daq::sso::get_token_from_device_code("sso-helper-test", "offline_access"));
    std::string refresh_token = result["refresh_token"];
    // save it
    ...
    // load it
    std::string new_token = daq::sso::get_token_from_refresh_token("sso-helper-test", refresh_token);
```

## Parsing the results

A helper routine is available to parse the result which is in JSON format. This way the user
does not have to parse it himself and can treat the tokens as opaque strings.

```c++
    // Assume the result of one of the above calls is available in an environment variable
    // or a file.
    auto result = getenv("SSO_TOKENS");
    // OR
    std::ifstream f("/home/rhauser/.cache/myapp/tokens.txt");
    std::string result;
    f.getline(result);

    daq::sso::Tokens tokens("sso-helper-test");
    daq::sso::parse_tokens(result, tokens);
```

The `Tokens` structure contains the following members:

```c++
    struct Tokens {
        std::string client_id;  // if set from constructor or manually
        std::string access_token;
        std::string refresh_token;
        std::string id_token;
        std::string scope;
        time_t      access_token_expires_at;
        time_t      refresh_token_expires_at;
    };
```

When a request is made, check the expiry date of  the access token:

```c++

std::string do_request(const std::string& url)
{
    // if less than 30s left
    if(tokens.access_token_expires_at < time(nullptr) + 30) {
       auto result = daq::sso::get_token_from_refresh_token(client_id, refresh_token);
       daq::sso::parse_tokens(result, tokens);
    }

    curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
    curl_easy_setopt(handle, CURLOPT_XOAUTH2_BEARER, tokens.access_token.c_str());

    // my_handler reads returned bytes and puts it into 'result'
    std::string result;
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, my_handler);

    auto rc = curl_easy_perform(handle);
    if(rc == CURLE_OK)
        std::cout << result;
}
```

A helper routine in the `Tokens` class can do the refreshing if you have set
the `client_id` field either via the constructor or manually:

```c++
   daq::sso::Tokens tokens("sso-helper-test");
   std::string result = ... // get initial tokens
   daq::sso::parse_tokens(result, tokens);

   ... // as above
   curl_easy_setopt(handle, CURLOPT_XOAUTH2_BEARER, tokens.get_valid_access_token().c_str());
   ... // as above
```
