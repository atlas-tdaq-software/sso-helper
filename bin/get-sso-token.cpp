/* SPDX-License-Identifier: Apache-2.0 */

#include "sso-helper/sso-helper.h"

#include <cstdlib>
#include <memory>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <sys/stat.h>

#include <nlohmann/json.hpp>

static void usage()
{
    std::cerr
        << "Acquire token for client\n\n"
        << " get-sso-token krb      -c <client-id> -u <redirect_uri> [ -o <output-file> ] [ -s '<scopes...>' ] [ -j <cookie-jar> ] [-A|-R]\n"
        << " get-sso-token browser  -c <client-id> -u <redirect_uri> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]\n"
        << "   Note: One of the valid redirection URLs *must* be http://localhost for browser option\n"
        << " get-sso-token secret   -c <client-id> -p <client-secret> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]\n"
        << " get-sso-token device   -c <client-id> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]\n"
        << " get-sso-token password -c <client-id> [-U <username> ] [-P <password>] [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]\n"
        << " get-sso-token auto     -c <client-id> [ -u <redirect_uri> ] [ -o <output-file> ] [ -s '<scopes...>' ] [ -j <cookie-jar> ] [-A|-R]\n"
        << "   Note: tries krb, browser, device in turn\n"
        << " get-sso-token api      -c <client-id> -p <client-secret> -t <target-client-id> [ -o <output-file> ] [ -s '<scopes...>' ] [-A|-R]\n\n"
        << "Refresh token\n\n"
        << " get-sso-token refresh  -c <client-id> -r <refresh-token>  [ -o <output-file> ] [-A|-R]\n"
        << "   Note: Use '-r -' to read the refresh token from stdin\n\n"
        << "Exchange token\n\n"
        << " get-sso-token exchange -c <client-id> -p <client-secret> -t <target-client -T <access_token> [-A|-R]\n"
        << "   Note: Use '-T -' to read the access token from stdin\n\n"
        << " -A Only print access token from result\n"
        << " -R Only print refresh token from results\n\n"
        << " The default cookie jar is /tmp/sso-cookies-$(id -u)\n\n"
        << "Show this help\n\n"
        << " get-sso-token -h\n";
}


int main(int argc, char *argv[])
{
    std::string how;
    std::string client_id;
    std::string redirect_uri;
    std::string output_file{"-"};
    std::string cookiefile{"/tmp/"};
    cookiefile += "sso-cookies-" + std::to_string(getuid());
    std::string scopes;
    std::string refresh_token;
    std::string target_client_id;
    std::string client_secret;
    std::string access_token;
    std::string metadata_url;
    std::string user;
    std::string password;

    bool at_only = false;
    bool rt_only = false;

    optind = 1;

    int opt;
    while((opt = getopt(argc, argv, "c:o:j:s:r:u:p:t:T:m:U:P:RAh")) != -1) {
        switch (opt) {
        case 'c':
            client_id = optarg;
            break;
        case 'o':
            output_file = optarg;
            break;
        case 'j':
            cookiefile = optarg;
            break;
        case 's':
            scopes = optarg;
            break;
        case 'r':
            refresh_token = optarg;
            break;
        case 'u':
            redirect_uri = optarg;
            break;
        case 'p':
            client_secret = optarg;
            break;
        case 't':
            target_client_id = optarg;
            break;
        case 'T':
            access_token = optarg;
            break;
        case 'A':
            at_only = true;
            break;
        case 'R':
            rt_only = true;
            break;
        case 'm':
            metadata_url = optarg;
            break;
        case 'U':
            user = optarg;
            break;
        case 'P':
            password = optarg;
            break;
        case 'h':
            usage();
            exit(0);
            break;
        default:
            usage();
            exit(EXIT_FAILURE);
            break;
        }
    }

    if(optind < argc) {
        how = argv[optind];
    } else {
        usage();
        exit(EXIT_FAILURE);
    }

    try {
        std::string result;

        if(!metadata_url.empty()) {
            daq::sso::set_metadata(metadata_url);
        }

        if (how == "krb") {
            if(client_id.empty() || redirect_uri.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token_from_kerberos(client_id, redirect_uri, cookiefile, scopes);
        } else if (how == "browser") {
            if(client_id.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token_from_browser(client_id, client_secret, scopes);
        } else if (how == "password") {
            if(client_id.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            if(user.empty()) {
                user =  getenv("USER") ? getenv("USER") : "nobody";
            }

            if(password.empty()) {
                password = getpass("Password: ");
            } else if(password == "-") {
                getline(std::cin, password);
            }
            result = daq::sso::get_token_from_password(client_id, user, password, client_secret, scopes);
        } else if (how == "device") {
            if(client_id.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token_from_device_code(client_id, client_secret, scopes);
        } else if (how == "secret") {
            if(client_id.empty() || client_secret.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token_from_client_secret(client_id, client_secret, scopes);
        } else if (how == "api") {
            if(client_id.empty() || client_secret.empty() || target_client_id.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token_from_auth_api(client_id, client_secret, target_client_id, scopes);
        } else if (how == "refresh") {
            if(client_id.empty() || refresh_token.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            if(refresh_token == "-") {
                getline(std::cin, refresh_token);
            }
            result = daq::sso::get_token_from_refresh_token(client_id, refresh_token, client_secret);
        } else if (how == "exchange") {
            if(client_id.empty() || client_secret.empty() || target_client_id.empty() || access_token.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            if (access_token == "-") {
                getline(std::cin, access_token);
            }
            result = daq::sso::exchange_token(client_id, client_secret, access_token, target_client_id);
        } else if (how == "auto") {
            if(client_id.empty()) {
                usage();
                exit(EXIT_FAILURE);
            }
            result = daq::sso::get_token(client_id, redirect_uri, cookiefile, scopes);
        }

        if(at_only) {
            try {
                result = nlohmann::json::parse(result)["access_token"];
            } catch (...) {}
        }

        if(rt_only) {
            try {
                result = nlohmann::json::parse(result)["refresh_token"];
            } catch (...) {}
        }

        if(output_file == "-") {
            std::cout << result;
        } else {
            std::ofstream of{output_file.c_str()};
            of << result;
            chmod(output_file.c_str(), S_IRUSR | S_IWUSR );
        }

        return EXIT_SUCCESS;

    } catch (daq::sso::SSOException& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        return EXIT_FAILURE;
    }
}
