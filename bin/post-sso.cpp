/* SPDX-License-Identifier: Apache-2.0 */

#include "sso-helper/sso-helper.h"

#include <memory>
#include <iostream>
#include <fstream>
#include <unistd.h>

#include <sys/stat.h>

static void usage()
{
    std::cerr
        << "Post to access SSO protected webpage and store cookies\n\n"
        << "  post-sso -u <url> [-q] [ -o <output-file> ] [ -j <cookie-jar> ]\n"
        << "   -q do not print any output\n"
        << "   -u <url>         the URL to retrieve\n"
        << "   -j <cookie-jar>  specify where to store cookies (default /tmp/sso-cookies-$(id -u)\n"
        << "   -o <output-file> print output into file. Use '-o -' to print to stdout (default)\n"
        << "   -H <header-name>=<header-value> add HTTP header\n"
        << "   -T <access-token> adds necessary authorization header\n"
        << "   -d <data>\n"
        << "      <data> can be\n"
        << "        URLencoded data\n"
        << "        @<filename>\n\n"
        << "Show this help\n"
        << "  get-sso-cookies -h\n\n";
}

std::string
get_data(const std::string& data)
{
    std::string post_data;
    if(!data.empty() && data[0] == '@') {
        std::ifstream f(data.substr(1));
        std::string line;
        while(getline(f, line)) {
            post_data += line;
        }
    } else {
        post_data = data;
    }
    return post_data;
}

int main(int argc, char *argv[])
{
    std::string output_file{"-"};
    std::string cookiefile{"/tmp/"};
    std::string url;
    std::string data;
    std::vector<std::string> headers;
    std::string token;
    bool quiet = false;
    cookiefile += "sso-cookies-" + std::to_string(getuid());

    optind = 1;

    int opt;
    while((opt = getopt(argc, argv, "o:j:u:d:H:T:hq")) != -1) {
        switch (opt) {
        case 'o':
            output_file = optarg;
            break;
        case 'j':
            cookiefile = optarg;
            break;
        case 'u':
            url = optarg;
            break;
        case 'q':
            quiet = true;
            break;
        case 'd':
            if(data.empty()) {
                data = get_data(optarg);
            } else {
                data += '&' + get_data(optarg);
            }
            break;
        case 'T':
            token = optarg;
            break;
        case 'H':
            headers.emplace_back(optarg);
            break;
        case 'h':
            usage();
            exit(0);
            break;
        default:
            usage();
            exit(EXIT_FAILURE);
            break;
        }
    }

    if(url.empty()) {
        usage();
        exit(EXIT_FAILURE);
    }

    if(!token.empty()) {
        headers.emplace_back("authorization: bearer " + token);
    }

    try {
        std::string result = daq::sso::post(url, data, cookiefile, headers);
        if(!quiet) {
            if(output_file == "-") {
                std::cout << result;
            } else if(!output_file.empty()) {
                std::ofstream f(output_file);
                f << result;
            }
            chmod(cookiefile.c_str(), S_IRUSR | S_IWUSR );
        }
    } catch (daq::sso::SSOException& ex) {
        std::cerr << "Error: " << ex.what() << std::endl;
        chmod(cookiefile.c_str(), S_IRUSR | S_IWUSR );
        return EXIT_FAILURE;
    }
}
