# source me, don't execute me !

#
# Acquiring tokens:
#
#   token_from_device_code <client-id> [ <scope> ]
#   token_from_refresh_token <client-id> <refresh-token>
#   token_from_client_secret <client-id> <client-secret>
#   token_from_browser <client-id> [ <scope> ]
#
# These functions return the authentication server's response,
# a JSON structure that contains the tokens and other information.
# Typically you pass this to token_parse to have it all stored
# in easily accessible variables.
#
#   token_parse "$(token_from_device_code sso-helper-test)"
#
# The variables the user is concerned with are:
#   SSO_ACCESS_TOKEN - the access token
#   SSO_REFRESH_TOKEN - the refresh token
#   SSO_EXPIRES_AT   - access token expiration date in Unix timestamp format
#   SSO_REFRESH_EXPIRES_AT - refresh token expiration date
#   SSO_CLIENT_ID    - the client-id used in the last token acquired
#
# Use them:
#
#   curl --oauth2-bearer "$SSO_ACCESS_TOKEN" https://rhauser-c9s.cern.ch/api/info
#   date -d @${SSO_EXPIRES_AT)
#   token_parse $(token_from_refresh_token sso-helper-test "$SSO_REFRESH_TOKEN")
#
# Other helper functions
#
#   token_valid [ <client-id> ] - make sure that access token is valid, refresh if necessary (and possible)
#      if SSO_CLIENT_ID is set you can  call it without argument
#      you can simple call this before every curl invocation: token_valid && curl --oauth2-bearer "$SSO_ACCESS_TOKEN" ....
#   token_decode [-h|-p] "$SSO_ACCESS_TOKEN" - parse header and/or payload of token
#   token_verify [-q] "$SSO_ACCESS_TOKEN" [ audience ] - verify token, print OK if successful unless -q is given, error otherwise
#   token_revoke <client-id> <token>  - revoke token, typically used with refresh token
#   token_userinfo <client-id> <token> - get user info
#
# For long running tasks (e.g. cron jobs) acquire an offline token:
#    token_from_device_code sso-helper-test offline_access > offline.token
# Then later:
#    token_parse "$(cat offline.token)"
#    token_valid && curl --oauth2-bearer "$SSO_ACCESS_TOKEN" https://rhauser-c9s.cern.ch/api/info

# To use a different IDP:
# Set this variable before sourcing this file, or
# call 'metadata_read [ <URL> ]' explicitly
SSO_METADATA_URL=${SSO_METADATA_URL:=https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration}

metadata_read()
{
    SSO_METADATA=$(curl -s ${1:-${SSO_METADATA_URL}})
    SSO_TOKEN_ENDPOINT=${SSO_TOKEN_ENDPOINT:=$(echo "$SSO_METADATA" | jq -r .token_endpoint)}
    SSO_AUTH_ENDPOINT=${SSO_AUTH_ENDPOINT:=$(echo "$SSO_METADATA" | jq -r .authorization_endpoint)}
    SSO_DEVICE_AUTH_ENDPOINT=${SSO_DEVICE_AUTH_ENDPOINT:=$(echo "$SSO_METADATA" | jq -r .device_authorization_endpoint)}
    SSO_REVOCATION_ENDPOINT=${SSO_REVOCATION_ENDPOINT:=$(echo "$SSO_METADATA" | jq -r .revocation_endpoint)}
    SSO_USERINFO_ENDPOINT=${SSO_USERINFO_ENDPOINT:=$(echo "$SSO_METADATA" | jq -r .userinfo_endpoint)}
    SSO_PUBLIC_KEY=$(curl -s $(echo "$SSO_METADATA" | jq -r .jwks_uri) | jq -r '.keys[] | select(.alg=="RS256") | .x5c[0]' | base64 -d | openssl x509 -noout -pubkey -inform der)
}

metadata_read

# Generate a code verifier. For PKCE length must be at least 32.
# Can also be used as 'state' parameter in code flow.
code_verifier()
{
    openssl rand ${1:-32} | basenc --base64url | tr -d '=\n'
}

# Generate the code challenge from the verifier.
# Pass the code verifier as argument to this for PKCE challenge
code_challenge()
{
    [ $# -eq 1 ] || { echo "usage: challenge <string>" >&2; return 1; }
    echo -n "$1" | openssl sha256 -binary | basenc --base64url | tr -d '=\n'
}

# Parse the response from auth server, ie. what is returned
# by one of the token_from_XXX() functions below
#
# Argument: <sso-response>
#
# Sets various env variables
#   SSO_ACCESS_TOKEN
#   SSO_EXPIRES_AT    (absolute expiration date in Unix epoch seconds)
#   SSO_REFRESH_TOKEN (if exists)
#   SSO_REFRESH_EXPIRES_AT (if exists)
#     if offline refresh token, this is set to one month in the future
#
token_parse()
{
    local err
    [ $# -eq 1 ] || { echo "usage: token_parse <sso-response>" >&2; return 1; }
    err=$(echo "$1" | jq -r .error)
    if [ "${err}" != "null" ]; then
        SSO_ACCESS_TOKEN=""
        # SSO_REFRESH_TOKEN=""
        return 2
    fi
    SSO_ACCESS_TOKEN=$(echo "$1" | jq -r .access_token)
    SSO_REFRESH_TOKEN=$(echo "$1" | jq -r .refresh_token)
    SSO_EXPIRES_AT=$(( $(date +%s) +  $(echo "$1" | jq -r .expires_in) ))
    SSO_ID_TOKEN=$(echo "$1" | jq -r .id_token)
    if [ "$(echo "$1" | jq -r .refresh_expires_in)" = "0" ]; then
        SSO_REFRESH_EXPIRES_AT=$(( $(date +%s) + 30 * 24 * 3600 ))
    else
        SSO_REFRESH_EXPIRES_AT=$(( $(date +%s) +  $(echo "$1" | jq -r .refresh_expires_in) ))
    fi
}

# Decode a token given as argument (without verification, see token_verify() for that !)
#
# Arguments: [-h|-p] <token> [ <jq-option>...]
#
# By default decodes both header and payload
#
# -h : decode only header
# -p : decode only payload
#
# Any additional arguments are passed as is to 'jq'
#
token_decode()
{
    local h p t
    while [ $# -gt 1 ]; do
        case "$1" in
            -h)
                p=0
                shift
                ;;
            -p)
                h=0
                shift
                ;;
            *)
                break
                ;;
        esac
    done
    [ $# -ge 1 ] || { echo "usage: [-p|-h] token_decode <string> [ <jq-options> ]"; return 1; }
    t="$1"
    shift
    [ "$h" = "0" ] || echo "$t" | cut -d. -f1 | basenc -d --base64url 2> /dev/null | jq "$@"
    [ "$p" = "0" ] || echo "$t" | cut -d. -f2 | basenc -d --base64url 2> /dev/null | jq "$@"
}

# return status
# 0 - success
# 1 - wrong usage
# 2 - curl error
# 3 - auth error
#
# arguments: <client-id> [ <scope> ]
token_from_device_code()
{
    [ $# -ge 1 ] || { echo "usage: token_from_device_grant <client> [ <scope> ]" >&2; return 1; }
    local v c r e interval n
    v=$(code_verifier)
    c=$(code_challenge $v)
    n=$(code_verifier)

    r=$(curl -s ${SSO_DEVICE_AUTH_ENDPOINT} \
             -d "client_id=$1" \
             -d "code_challenge=${c}" \
             -d "code_challenge_method=S256" \
             -d "nonce=${n}" \
             -d "scope=openid"${2:+"%20$2"}
     )
    [ $? -ne 0 ] && return 2
    e=$(echo $r | jq -r .error 2> /dev/null)
    [ "${e}" != "null" ] && { echo $r | jq -r .error_description >&2 ; return 3; }

    local user_code device_code verification_uri verification_uri_complete

    user_code=$(echo $r | jq -r .user_code)
    device_code=$(echo $r | jq -r .device_code)
    verification_uri=$(echo $r | jq -r .verification_uri)
    verification_uri_complete=$(echo $r | jq -r .verification_uri_complete)
    interval=$(echo $r | jq -r .interval)

    (echo "Open this URL in a local browser:"
    echo
    echo "   ${verification_uri}"
    echo
    echo " and enter this code:"
    echo
    echo "   ${user_code}"
    echo
    echo "Or open directly this URL:"
    echo
    echo "   ${verification_uri_complete}"
    echo
    command -v qrencode > /dev/null && echo ${verification_uri_complete} | qrencode -o - -t UTF8i  2> /dev/null) >&2

    e="authorization_pending"
    while [ "${e}" = "authorization_pending" -o "${e}" = "slow_down" ]
    do
        if [ "${e}" = "slow_down" ]; then
            interval=$(( interval + 5 ))
        fi
        sleep ${interval}
        r=$(curl -s ${SSO_TOKEN_ENDPOINT} \
             -d "grant_type=urn:ietf:params:oauth:grant-type:device_code" \
             -d "client_id=$1" \
             -d "code_verifier=${v}" \
             -d "device_code=${device_code}")
        [ $? -ne 0 ] && return 2
        e=$(echo $r | jq -r .error)
    done

    if [ "$e" != "null" ]; then
       echo $r
       return 3
    fi

    SSO_CLIENT_ID=$1
    echo $r
}

# arguments: <client-id> <client-secret>
token_from_client_secret()
{
    [ $# -eq 2 ] || { echo "token_from_client_secret <client-id> <client-secret>" >&2; return 1; }
    SSO_CLIENT_ID=$1
    curl -s ${SSO_TOKEN_ENDPOINT} \
         -d "grant_type=client_credentials" \
         -d "client_id=$1" \
         -d "client_secret=$2"
}

# Refresh an access token
#
# Arguments: <client-id> <refresh-token>
token_from_refresh_token()
{
    [ $# -eq 2 ] || { echo "token_from_refresh_token <client-id> <refresh-token>" >&2; return 1; }
    SSO_CLIENT_ID=$1
    curl -s ${SSO_TOKEN_ENDPOINT} \
         -d "grant_type=refresh_token" \
         -d "client_id=$1" \
         -d "refresh_token=$2" \
         ${3:+-d "client_secret=$3"}
}

# token_from_browser <client-id> [ <scope> ]
token_from_browser()
{
    [ $# -ge 1 ] || { echo "usage: token_from_browser <client-id> [ <scope> ]" >&2; return 1; }
    local v c s n
    s=$(code_verifier)
    v=$(code_verifier)
    c=$(code_challenge $v)
    n=$(code_verifier)
    out=$(mktemp)
    ncat -l 5555 -C > ${out} <<EOF &
HTTP/1.1 200 Ok
content-type: text/html
connection: close

<html><head><title>Ok</title></head>
<body><h1>You can close me</h1></body></html>
EOF
    xdg-open "${SSO_AUTH_ENDPOINT}?response_type=code&client_id=$1&state=${s}&scope=openid${2:+%20$2}&redirect_uri=http://localhost:5555&code_challenge=${c}&code_challenge_method=S256&nonce=${n}" > /dev/null
    wait $!
    reply_state=$(grep -o '?state=[^&]*' ${out} | sed 's;?state=;;')
    code=$(grep -o 'code=[^ ]*' ${out} | sed 's;code=;;')
    rm -rf ${out}
    SSO_CLIENT_ID=$1
    if [ "${s}" = "${reply_state}" ]; then
        curl -s ${SSO_TOKEN_ENDPOINT} -d grant_type=authorization_code -d client_id=$1 -d code="${code}" -d code_verifier="${v}" -d redirect_uri=http://localhost:5555
    else
        return 1
    fi
}

# token_from_password <client-id> [ <scope> ]
token_from_password()
{
    [ $# -ge 1 ] || { echo "usage: token_from_password <client-id> [ <scope> ]" >&2; return 1; }
    password=$(echo "GETPIN" | pinentry | grep '^D ' | sed 's;^D ;;')
    curl -s ${SSO_TOKEN_ENDPOINT} -d grant_type=password -d client_id=$1 -d username=$USER -d password="${password} -d scope=openid${2:+ $2}"
}

# Make sure access token is valid, if neccessary refresh it and update variables
#
# Arguments: [ <client-id> ]  (if not set or different from $SSO_CLIENT_ID)
token_valid()
{
    if [ "${SSO_EXPIRES_AT}" -lt $(( $(date +%s) - 30 )) ]; then
        if rsp=$(token_from_refresh_token ${1:-${SSO_CLIENT_ID}} ${SSO_REFRESH_TOKEN})
        then
            token_parse "${rsp}"
        fi
    fi
}

# Verify token
#
# RS256 algo is hard-coded
#
# Arguments: <token> [ <audience> ]
#
# returns:
#  0 token verifies and is valid
#  1 token does not verify
#  2 token is expired, or not yet valid
#  3 audience is wrong
#  4 wrong number of arguments
#
token_verify()
{
    local pubkey sigfile r p
    local ECHO=echo
    if [ "${1}" = "-q" ]; then ECHO=: ; shift; fi
    [ $# -lt 1 ] && { echo "token_verify [-q] TOKEN [ AUDIENCE ]" >&2; return 4; }
    pubkey=$(mktemp)
    sigfile=$(mktemp)
    echo "$SSO_PUBLIC_KEY" > $pubkey
    echo -n "${1}" | cut -d. -f3 | basenc -d --base64url 2> /dev/null > $sigfile
    echo -n "${1}" | cut -d. -f1,2 | tr -d '\n' | openssl dgst -verify $pubkey -signature $sigfile -sha256 > /dev/null
    r=$?
    rm -f ${pubkey} ${sigfile}
    [ $r -ne 0 ] && { $ECHO "Verification failed"; return $r ; }
    p=$(echo "${1}" | cut -d. -f2 | basenc -d --base64url 2> /dev/null)

    local iat exp aud now
    aud=$(echo "$p" | jq -r .aud)
    iat=$(echo "$p" | jq -r .iat)
    exp=$(echo "$p" | jq -r .exp)

    now=$(date +%s)
    [ ${now} -lt ${iat} ] && { $ECHO "Token not yet valid" ; return 2 ; }
    [ ${now} -gt ${exp} ] && { $ECHO "Token expired"; return 2 ; }
    # [ $(echo "${aud}" | jq -r 'contains(["'${2:-${SSO_CLIENT_ID}}'"])') != "true" ] && { $ECHO "Wrong audience: \"${aud}\" instead of \"$2\""; return 3 ; }
    [ "${aud}" != "${2:-${SSO_CLIENT_ID}}" ] && { $ECHO "Wrong audience: \"${aud}\" instead of \"$2\""; return 3 ; }
    $ECHO "OK"
    return 0
}

# Revoke token
#
# Arguments: client-id token
#
# This works with both access and refresh tokens. However, if access tokens are verified locally
# it has essentially no real effect.
#
# In practice this can be used to invalidate a refresh token that got exposed.
token_revoke()
{
    [ $# -eq 2 ] || { echo "usage: token_revoke <client-id> <refresh-token>" >&2; return 1; }
    curl -f -s -d "client_id=$1" -d "token=$2" ${SSO_REVOCATION_ENDPOINT}
}

# Get user info, this requires the 'openid' scope
#
# Arguments: access-token
#
token_userinfo()
{
    [ $# -eq 1 ] || { echo "usage: token_userinfo <access-token>" >&2; return 1; }
    curl -f -s --oauth2-bearer "$1" $SSO_USERINFO_ENDPOINT
}
