# source me, don't execute me ! I'm a -*- bash -*- script
#
# Bash helper functions to use OAuth tokens and keep them up-to-date
#
# All state information is kept in environment variables
#
# Functions
#   sso-init-token      : initialize the token
#                         this is the only function requiring get-sso-token binary
#   sso-curl-with-token : do a HTTP request with the token
#   sso-check           : check if token is valid, refresh it if not

# Variables set by user:
#   SSO_COOKIE_FILE  :  a private cookie jar, else an internal default is used

# Variables
#   SSO_STATUS       : 0 if the last sso-... function was successful
#   SSO_CLIENT       : the client ID specified in sso-init-token
#   SSO_REDIRECT     : the redirect URI given in sso-init-token, default http://localhost
#   SSO_ACCESS_TOKEN : the current access token
#   SSO_REFRESH_TOKEN: the current refresh token
#   SSO_EXPIRES_AT   : access token expiration date in Unix seconds since epoch
#                      compare against `date +%s` to check
#   SSO_RESPONSE     : the full last response
#
# Long lived refresh tokens
#
# % sso-init <method> -c <client> -u <redirect_uri> -s offline_access
# % echo -n $SSO_REFRESH_TOKEN > saved.token
#
# To use it later
#
# % export SSO_CLIENT=<client>
# % export SSO_REFRESH_TOKEN=$(cat saved.token)
# % sso-curl-with-token <url>
#

# Variables
export SSO_STATUS SSO_RESPONSE SSO_ACCESS_TOKEN SSO_REFRESH_TOKEN SSO_CLIENT SSO_REDIRECT SSO_EXPIRES_AT

export SSO_COOKIE_FILE=${SSO_COOKIE_FILE:=/tmp/sso-cookies-$(id -u)}
touch ${SSO_COOKIE_FILE} && chmod og-rwx ${SSO_COOKIE_FILE}

# helper
sso-parse-response()
{
    [ -z "${SSO_RESPONSE}" ] && { SSO_EXPIRES_AT=""; SSO_ACCESS_TOKEN=""; return 1; }
    SSO_EXPIRES_AT=$(( $(echo -n $SSO_RESPONSE | jq -r .expires_in) + $(date +%s) ))
    SSO_ACCESS_TOKEN=$(echo -n $SSO_RESPONSE | jq -r .access_token)
    SSO_REFRESH_TOKEN=$(echo -n $SSO_RESPONSE | jq -r .refresh_token)
}

# sso-init-token <method> <client-id> [ <uri[=http://localhost>] ] [options...]
sso-init-token()
{
    [ $# -lt 2 ] && { echo "sso-init-token <method> <client-id> [ <uri> [ options...] ]" >&2 ; SSO_STATUS=1; return $SSO_STATUS; }
    SSO_CLIENT=$2
    SSO_REDIRECT=${3:-http://localhost}
    SSO_RESPONSE=$(get-sso-token $1 -c $SSO_CLIENT -u $SSO_REDIRECT ${SSO_COOKIE_FILE} $@)
    SSO_STATUS=$?
    [ $SSO_STATUS -eq 0 ] && { sso-parse-response; SSO_STATUS=$? ; }
    return $SSO_STATUS
}

sso-check()
{
    SSO_STATUS=0
    if [ $(date +%s) -gt $(( ${SSO_EXPIRES_AT:=0} - 20 )) ]; then
        SSO_RESPONSE=$(curl -s https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token \
                            -d grant_type=refresh_token \
                            -d client_id=${SSO_CLIENT}  \
                            -d refresh_token=${SSO_REFRESH_TOKEN})
        SSO_STATUS=$?
        [ $SSO_STATUS -eq 0 ] && { sso-parse-response; SSO_STATUS=$? ; }
    fi
    return $SSO_STATUS
}

# url [ <options>... ]
sso-curl-with-token()
{
    sso-check && curl -H "Authorization: Bearer $SSO_ACCESS_TOKEN" -c ${SSO_COOKIE_FILE} -b ${SSO_COOKIE_FILE} $@
}

