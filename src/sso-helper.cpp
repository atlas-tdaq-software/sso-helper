//* SPDX-License-Identifier: Apache-2.0 */
#include "sso-helper/sso-helper.h"

#include <cstdio>
#include <curl/curl.h>
#include <curl/easy.h>
#include <string>
#if LIBCURL_VERSION_MAJOR >= 7 && LIBCURL_VERSION_MINOR >= 62
#include <curl/urlapi.h>
#endif
#include <memory>
#include <fstream>
#include <iostream>
#include <sstream>
#include <cstdlib>

#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <netinet/in.h>

#include <openssl/rand.h>
#include <openssl/evp.h>

#include <nlohmann/json.hpp>

namespace daq {

    namespace sso {

        void
        Tokens::clear()
        {
            access_token.clear();
            refresh_token.clear();
            id_token.clear();
            scope.clear();
        }

        std::string
        Tokens::get_valid_access_token()
        {
            if(!access_token.empty() && time(nullptr) + before_expiration < access_token_expires_at)
                return access_token;
            if(!refresh_token.empty() && !client_id.empty()) {
                std::string result = get_token_from_refresh_token(client_id, refresh_token);
                if(parse_tokens(result, *this))
                    return access_token;
            }
            throw TokenError("No valid access or refresh token");
        }

        namespace {

            const std::string cern_meta_data_url{"https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration"};

            // meta data url, default is CERN
            std::string meta_data_url{"https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration"};

            // cached copy for CERN
            std::string cern_meta_data{
                R"({"issuer":"https://auth.cern.ch/auth/realms/cern","authorization_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth","token_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token","introspection_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token/introspect","userinfo_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo","end_session_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/logout","frontchannel_logout_session_supported":true,"frontchannel_logout_supported":true,"jwks_uri":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/certs","check_session_iframe":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/login-status-iframe.html","grant_types_supported":["authorization_code","implicit","refresh_token","password","client_credentials","urn:ietf:params:oauth:grant-type:device_code","urn:openid:params:grant-type:ciba","urn:ietf:params:oauth:grant-type:token-exchange"],"acr_values_supported":["0","1"],"response_types_supported":["code","none","id_token","token","id_token token","code id_token","code token","code id_token token"],"subject_types_supported":["public","pairwise"],"id_token_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512"],"id_token_encryption_alg_values_supported":["RSA-OAEP","RSA-OAEP-256","RSA1_5"],"id_token_encryption_enc_values_supported":["A256GCM","A192GCM","A128GCM","A128CBC-HS256","A192CBC-HS384","A256CBC-HS512"],"userinfo_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512","none"],"userinfo_encryption_alg_values_supported":["RSA-OAEP","RSA-OAEP-256","RSA1_5"],"userinfo_encryption_enc_values_supported":["A256GCM","A192GCM","A128GCM","A128CBC-HS256","A192CBC-HS384","A256CBC-HS512"],"request_object_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512","none"],"request_object_encryption_alg_values_supported":["RSA-OAEP","RSA-OAEP-256","RSA1_5"],"request_object_encryption_enc_values_supported":["A256GCM","A192GCM","A128GCM","A128CBC-HS256","A192CBC-HS384","A256CBC-HS512"],"response_modes_supported":["query","fragment","form_post","query.jwt","fragment.jwt","form_post.jwt","jwt"],"registration_endpoint":"https://auth.cern.ch/auth/realms/cern/clients-registrations/openid-connect","token_endpoint_auth_methods_supported":["private_key_jwt","client_secret_basic","client_secret_post","tls_client_auth","client_secret_jwt"],"token_endpoint_auth_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512"],"introspection_endpoint_auth_methods_supported":["private_key_jwt","client_secret_basic","client_secret_post","tls_client_auth","client_secret_jwt"],"introspection_endpoint_auth_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512"],"authorization_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512"],"authorization_encryption_alg_values_supported":["RSA-OAEP","RSA-OAEP-256","RSA1_5"],"authorization_encryption_enc_values_supported":["A256GCM","A192GCM","A128GCM","A128CBC-HS256","A192CBC-HS384","A256CBC-HS512"],"claims_supported":["aud","sub","iss","auth_time","name","given_name","family_name","preferred_username","email","acr"],"claim_types_supported":["normal"],"claims_parameter_supported":true,"scopes_supported":["openid","groups","acr","offline-access","phone","building","offline_access","external","authz-roles","identity-provider","cern-login-info","roles","oidc-web-origins","web-origins","email","department","cern-kerberos-principal","profile","microprofile-jwt","client-id","address"],"request_parameter_supported":true,"request_uri_parameter_supported":true,"require_request_uri_registration":true,"code_challenge_methods_supported":["plain","S256"],"tls_client_certificate_bound_access_tokens":true,"revocation_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/revoke","revocation_endpoint_auth_methods_supported":["private_key_jwt","client_secret_basic","client_secret_post","tls_client_auth","client_secret_jwt"],"revocation_endpoint_auth_signing_alg_values_supported":["PS384","ES384","RS384","HS256","HS512","ES256","RS256","HS384","ES512","PS256","PS512","RS512"],"backchannel_logout_supported":true,"backchannel_logout_session_supported":true,"device_authorization_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth/device","backchannel_token_delivery_modes_supported":["poll","ping"],"backchannel_authentication_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/ext/ciba/auth","backchannel_authentication_request_signing_alg_values_supported":["PS384","ES384","RS384","ES256","RS256","ES512","PS256","PS512","RS512"],"require_pushed_authorization_requests":false,"pushed_authorization_request_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/ext/par/request","mtls_endpoint_aliases":{"token_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token","revocation_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/revoke","introspection_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/token/introspect","device_authorization_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/auth/device","registration_endpoint":"https://auth.cern.ch/auth/realms/cern/clients-registrations/openid-connect","userinfo_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/userinfo","pushed_authorization_request_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/ext/par/request","backchannel_authentication_endpoint":"https://auth.cern.ch/auth/realms/cern/protocol/openid-connect/ext/ciba/auth"}})"};

            // for easier use
            std::string issuer;
            std::string authorization_endpoint;
            std::string device_authorization_endpoint;
            std::string token_endpoint;
            std::string jwks_uri;

            std::string auth_host;

            // PKCE helper functions

            // Let openssl convert the data to base64
            // then fix the two special characters and padding
            std::string
            base64url_encode(const unsigned char *in, size_t in_size)
            {
                std::string out((in_size/3 + 1) * 4 + 1, '\0');
                size_t size = EVP_EncodeBlock((unsigned char *)out.c_str(), in, in_size);
                std::replace(out.begin(), out.end(), '+', '-');
                std::replace(out.begin(), out.end(), '/', '_');
                size_t pos = out.find('=');
                if(pos != std::string::npos) {
                    out.erase(pos);
                }
                return out.substr(0, std::min(size, out.size()));
            }

            // Create a base64url encoded string from 32 random bytes
            // suitable for PKCE code verify, but also 'state' parameter.
            std::string
            get_code_verifier()
            {
                unsigned char buf[32];
                RAND_bytes(&buf[0], sizeof buf);
                return base64url_encode(buf, sizeof buf);
            }

            // Get base64url encoded SHA256 digest of verifier
            std::string
            get_code_challenge(const std::string& verifier)
            {
                unsigned char digest[32];
                EVP_Digest(verifier.c_str(), verifier.size(), digest, nullptr, EVP_sha256(), nullptr);
                return base64url_encode(digest, sizeof digest);
            }

            // CURL helper functions

            // Callback function for CURL, save received text into string
            size_t
            receive_data(void *buffer, size_t size, size_t nmemb, void *userp)
            {
                std::string *key = reinterpret_cast<std::string*>(userp);
                key->append(reinterpret_cast<char *>(buffer), nmemb * size);
                return nmemb * size;
            }

            // Send constant char string to socket.
            int
            send_text(int s, const char *msg)
            {
                return send(s, msg, strlen(msg), 0);
            }

            inline void
            check_curl_result(CURLcode err)
            {
                if(err != CURLE_OK) {
                    throw CURLError(curl_easy_strerror(err));
                }
            }

            // Retrieve page at url, following redirections
            std::string
            get_page(CURL *handle, const std::string& url, long follow = 1L)
            {
                std::string page;

                curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
                curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
                curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
                curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, follow);
                check_curl_result(curl_easy_perform(handle));
                return page;
            }

            std::string
            post_page(CURL *handle,
                      const std::string& url,
                      const std::string& post_fields = {},
                      long follow = 1L)
            {
                std::string page;

                curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
                curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
                curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
                curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, follow);
                curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_fields.c_str());
                check_curl_result(curl_easy_perform(handle));

                return page;
            }

            // Retrieve meta data from provider
            void
            retrieve_metadata(CURL *handle, const std::string& url)
            {
#if LIBCURL_VERSION_MAJOR >= 7 && LIBCURL_VERSION_MINOR >= 62
                std::unique_ptr<CURLU, decltype(&curl_url_cleanup)> h{curl_url(), curl_url_cleanup};
                auto rc = curl_url_set(h.get(), CURLUPART_URL, url.c_str(), 0);
                if(rc) {
#if LIBCURL_VERSION_MAJOR >= 7 && LIBCURL_VERSION_MINOR >= 80
                    throw CURLError(curl_url_strerror(rc));
#else
                    throw CURLError("CURL URL parse error");
#endif
                }
                char *host{nullptr};
                rc = curl_url_get(h.get(), CURLUPART_HOST, &host, 0);
                if(rc) {
#if LIBCURL_VERSION_MAJOR >= 7 && LIBCURL_VERSION_MINOR >= 80
                    throw CURLError(curl_url_strerror(rc));
#else
                    throw CURLError("CURL URL parse error");
#endif
                      }
                std::unique_ptr<char, decltype(&curl_free)> hhost(host, curl_free);
#else
                // simple search, assume no funny stuff like port numbers etc.
                auto pos = url.find('/', url.find('/', url.find('/') + 1) + 1);
                std::string host = url.substr(8, pos - 8);
#endif
                nlohmann::json result;
                if(url != cern_meta_data_url)
                    result = nlohmann::json::parse(get_page(handle, url));
                else
                    result = nlohmann::json::parse(cern_meta_data);
                auth_host = host;
                issuer = result["issuer"];
                authorization_endpoint = result["authorization_endpoint"];
                device_authorization_endpoint = result["device_authorization_endpoint"];
                token_endpoint = result["token_endpoint"];
                jwks_uri = result["jwks_uri"];
            }

            bool
            check_metadata(CURL *handle)
            {
                if(authorization_endpoint.empty()) {
                    // allow global override via env variable
                    if(const char *env_meta_url = getenv("SSO_METADATA_URL")) {
                        if(meta_data_url != env_meta_url) {
                            meta_data_url = env_meta_url;
                        }
                    }
                    retrieve_metadata(handle, meta_data_url);
                }
                return !authorization_endpoint.empty();
            }

            std::string
            post_saml(CURL *handle, const std::string& page)
            {
                size_t idx = page.find("saml-post-binding");
                if(idx == std::string::npos) throw ProtocolError("saml-post-binding not found");

                size_t action_idx = page.find("action=", idx);
                if(action_idx == std::string::npos) throw ProtocolError("SAML action attribute not found");
                std::string action = page.substr(action_idx + 8, page.find('"', action_idx + 8) - action_idx - 8);

                size_t value_idx = page.find("value=", idx);
                if(value_idx == std::string::npos) throw ProtocolError("SAML value attribute not found");
                std::string value = page.substr(value_idx + 7, page.find('"', value_idx + 7) - value_idx - 7);

                std::unique_ptr<char, decltype(&curl_free)> escaped{curl_easy_escape(handle, value.c_str(), value.size()), curl_free};
                std::string data{"SAMLResponse="};
                data += escaped.get();
                curl_easy_setopt(handle, CURLOPT_URL, action.c_str());
                curl_easy_setopt(handle, CURLOPT_POSTFIELDS, data.c_str());

                std::string result;
                curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
                curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
                curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 0L);

                check_curl_result(curl_easy_perform(handle));

                const char *url;
                curl_easy_getinfo(handle, CURLINFO_REDIRECT_URL, &url);

                return url ? url : "";
            }

        } // anonymous namespace

        std::string
        do_kerberos_login(CURL *handle,
                          const std::string& page, // the auth server login page content
                          const std::string& redirect_uri)
        {
            // For SAML we might have been redirected to the auth server, but
            // if we are already logged into a session we just have to do the
            // final step
            if(page.find("SAMLResponse") != std::string::npos) {
                return post_saml(handle, page);
            }

            // hack to avoid parsing the full HTML page
            // 1. find 'id="social-kerberos"' in page
            // 2. rfind '<a' starting from 1.
            // 3. find 'href="' starting from 2
            size_t idx = page.find(" id=\"social-kerberos\"");
            if(idx == std::string::npos) throw ProtocolError("social-kerberos id not found on login page");

            idx = page.rfind("<a ", idx);
            if(idx == std::string::npos) throw ProtocolError("kerberos URL not found on login page");

            idx = page.find(" href=\"", idx);
            if(idx == std::string::npos) throw ProtocolError("kerberos URL not found on login page");

            size_t end_idx = page.find("\"", idx + 8);
            if(end_idx == std::string::npos)  throw ProtocolError("kerberos URL not found on login page");

            std::string href{page.substr(idx + 8, end_idx - idx - 8)};

            idx = 0;
            do {
                if((idx = href.find("&amp;", idx)) != std::string::npos) {
                    href.replace(idx, 5, "&");
                }
            } while(idx != std::string::npos);

            std::string url{"https://"};
            url += auth_host + '/' + href;

            std::string data;

            // Go to kerberos handling page
            curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &data);
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 0L);

            check_curl_result(curl_easy_perform(handle));

            long response_code;
            char *last_url = nullptr;

            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            curl_easy_getinfo(handle, CURLINFO_REDIRECT_URL, &last_url);

            // response 401 and last_url == nullptr typically means no valid kerberos ticket
            if(!last_url) throw ProtocolError("Kerberos ticket not valid ?");
            url = last_url;

            curl_easy_setopt(handle, CURLOPT_USERNAME, "");
            curl_easy_setopt(handle, CURLOPT_PASSWORD, "");
            curl_easy_setopt(handle, CURLOPT_HTTPAUTH, CURLAUTH_GSSNEGOTIATE);

            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 0L);
            curl_easy_setopt(handle, CURLOPT_URL, url.c_str());

            data.clear();
            check_curl_result(curl_easy_perform(handle));

            curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
            curl_easy_getinfo(handle, CURLINFO_REDIRECT_URL, &last_url);
            if(!last_url) throw ProtocolError("Kerberos ticket not valid ?");
            url = last_url;

            while((response_code == 302) &&
                  (url.find(redirect_uri) == std::string::npos)) {
                // std::cout << "last [" << response_code << "] redirect URL = " << url << std::endl;
                curl_easy_setopt(handle, CURLOPT_URL, url.c_str());
                data.clear();
                check_curl_result(curl_easy_perform(handle));

                curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
                curl_easy_getinfo(handle, CURLINFO_REDIRECT_URL, &last_url);
                if(last_url)
                    url = last_url;
                else
                    url = redirect_uri;
            }

            if(response_code == 200 && data.find("SAMLResponse") != std::string::npos) {
                url = post_saml(handle, data);
            }

            return url;
        }

        void
        set_cookie_file(CURL *handle,
                        const std::string& cookie_jar)
        {
            if(!cookie_jar.empty()) {
                int fd = open(cookie_jar.c_str(), O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);
                if(fd != -1) {
                    close(fd);
                }
            }
            // read and store cookies in file
            curl_easy_setopt(handle, CURLOPT_COOKIEFILE, cookie_jar.c_str());
            curl_easy_setopt(handle, CURLOPT_COOKIEJAR, cookie_jar.c_str());
        }

        std::string
        get(const std::string& url,
            const std::string& cookie_file,
            const std::vector<std::string>& headers)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();

            set_cookie_file(handle, cookie_file);
            check_metadata(handle);

            curl_slist * header_list = nullptr;
            if(!headers.empty()) {
                for(auto& hdr : headers) {
                    header_list = curl_slist_append(header_list, hdr.c_str());
                }
                curl_easy_setopt(handle, CURLOPT_HTTPHEADER, header_list);
            }

            std::unique_ptr<curl_slist, decltype(&curl_slist_free_all)> holder(header_list, curl_slist_free_all);

            // try to get the page
            // if cookies are valid this succeeds and we are done
            std::string result = get_page(handle, url);

            char *effective_url;
            curl_easy_getinfo(handle, CURLINFO_EFFECTIVE_URL, &effective_url);
            if(effective_url) {
                std::string s(effective_url);
                if(!s.starts_with("https://" + auth_host)) {
                    // if we have not been redirected to auth host, we are done
                    // or at least that's all we can do
                    return result;
                }
            }

            // else 'result' contains the login page
            result = do_kerberos_login(handle, result, url);

            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, "");
            curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);

            // get the page to set the cookies
            return get_page(handle, url);

        }

        std::string
        post(const std::string& url,
             const std::string& data,
             const std::string& cookie_file,
             const std::vector<std::string>& headers)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();

            set_cookie_file(handle, cookie_file);
            check_metadata(handle);

            curl_slist * header_list = nullptr;
            if(!headers.empty()) {
                for(auto& hdr : headers) {
                    header_list = curl_slist_append(header_list, hdr.c_str());
                }
                curl_easy_setopt(handle, CURLOPT_HTTPHEADER, header_list);
            }

            std::unique_ptr<curl_slist, decltype(&curl_slist_free_all)> holder(header_list, curl_slist_free_all);

            // try to post
            // if cookies are valid this succeeds and we are done
            std::string result = post_page(handle, url, data);

            char *effective_url;
            curl_easy_getinfo(handle, CURLINFO_EFFECTIVE_URL, &effective_url);
            if(effective_url) {
                std::string s(effective_url);
                if(!s.starts_with("https://" + auth_host)) {
                    // if we have not been redirected to auth host, we are done
                    // or at least that's all we can do
                    return result;
                }
            }

            // else 'result' contains the login page
            result = do_kerberos_login(handle, result, url);

            // get the page to set the cookies
            return post_page(handle, url, data);

        }

        // Get new token via kerberos ticket.
        std::string
        get_token_from_kerberos(const std::string& client_id,
                                const std::string& redirect_uri,
                                const std::string& cookie_file,
                                const std::string& scope)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();

            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }

            set_cookie_file(handle, cookie_file);
            check_metadata(handle);

            // Get random printable string
            std::string state{get_code_verifier()};

            std::string verifier{get_code_verifier()};
            std::string challenge{get_code_challenge(verifier)};

            std::string nonce{get_code_verifier()};

            // The OIDC auth URL
            std::string login_page{authorization_endpoint};
            login_page += "?client_id=" + client_id + "&response_type=code&state=" + state + "&redirect_uri=" + redirect_uri;
            login_page += "&code_challenge=" + challenge + "&code_challenge_method=S256" + "&nonce=" + nonce;
            login_page += "&scope=openid" + (scope.empty() ? "" : "%20" + scope);

            std::string url;
            std::string page = get_page(handle, login_page, 0);

            if(!page.empty()) {
                url = do_kerberos_login(handle, page, redirect_uri);
                if(url.empty()) {
                    throw ProtocolError("Could not authenticate with kerberos for unknown reason");
                }
            } else {
                char *redirect_url;
                curl_easy_getinfo(handle, CURLINFO_REDIRECT_URL, &redirect_url);
                if(redirect_url)
                    url = redirect_url;
                else {
                    throw ProtocolError("Did not get expected redirect URL");
                }
            }

            size_t error_idx = url.find("error_description=");
            if(error_idx != std::string::npos) {
                auto error_msg = url.substr(error_idx + 18, url.find('&', error_idx) - error_idx - 18);
                std::replace(error_msg.begin(), error_msg.end(), '+', ' ');
                std::unique_ptr<char, decltype(&curl_free)> unescaped(curl_easy_unescape(handle, error_msg.c_str(), error_msg.size(), nullptr), curl_free);
                throw ProtocolError(unescaped.get());
            }

            size_t state_idx = url.find("state=");
            if(state_idx == std::string::npos) {
                throw ProtocolError("Did not receive 'state' parameter");
            }

            size_t state_end = url.find("&", state_idx);
            std::string state_received = url.substr(state_idx + 6, state_end - state_idx - 6);
            if (state_received != state) {
                throw ProtocolError("The 'state' parameter is invalid");
            }

            size_t code_idx  = url.find("code=");
            if(code_idx == std::string::npos) {
                throw ProtocolError("Did not receive 'code' parameter");
            }
            size_t code_end = url.find("&", code_idx);
            std::string code = url.substr(code_idx + 5, code_end - code_idx - 5);

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());

            std::string post_data{"grant_type=authorization_code"};
            post_data += "&client_id=" + client_id;
            post_data += "&code=" + code;
            post_data += "&code_verifier=" + verifier;
            post_data += "&redirect_uri=" + redirect_uri;
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            page.clear();
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);

            check_curl_result(curl_easy_perform(handle));

            return page;
        }

        /// Refresh an existing token, get new access and refresh token
        std::string
        get_token_from_refresh_token(const std::string& client_id,
                                     const std::string& refresh_token,
                                     const std::string& client_secret)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }

            check_metadata(handle);

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());

            std::string post_data{"grant_type=refresh_token"};
            post_data += "&client_id=" + client_id;
            post_data += "&refresh_token=" + refresh_token;
            if(!client_secret.empty()) {
                post_data += "&client_secret=" + client_secret;
            }
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            std::string result;
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);

            check_curl_result(curl_easy_perform(handle));

            return result;
        }

        std::string get_token_from_browser(const std::string& client_id,
                                           const std::string& client_secret,
                                           const std::string& scope,
                                           const std::string& success_msg,
                                           const std::string& failure_msg)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();

            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }

            check_metadata(handle);

            int server = socket(PF_INET6, SOCK_STREAM, 0);
            if(server < 0) {
                throw TransportError("Cannot create socket");
            }

            sockaddr_in6 addr;
            memset(&addr, 0, sizeof(addr));
            addr.sin6_family = AF_INET6;
            addr.sin6_addr = IN6ADDR_LOOPBACK_INIT ;
            if(bind(server, (struct sockaddr *) &addr, sizeof(sockaddr_in6)) < 0) {
                close(server);
                throw TransportError("Cannot bind socket");
            }

            if(listen(server, 1) < 0) {
                close(server);
                throw TransportError("Cannot listen on socket");
            }

            socklen_t sock_length = sizeof(addr);
            getsockname(server, (sockaddr *)&addr, &sock_length);

            unsigned short port = ntohs(addr.sin6_port);

            // Get random printable string
            std::string random_state{get_code_verifier()};

            std::string verifier = get_code_verifier();
            std::string challenge = get_code_challenge(verifier);

            std::string nonce{get_code_verifier()};

            std::string authz_url{authorization_endpoint};
            authz_url += "?response_type=code&client_id=" + client_id +
                "&code_challenge_method=S256&code_challenge=" + challenge + "&nonce=" + nonce +
                "&scope=openid" + (scope.empty() ? "" : "%20" + scope) +
                "&state=" + random_state + "&redirect_uri=http://localhost:" + std::to_string(port);
            if(!client_secret.empty()) {
                authz_url += "&client_secret=" + client_secret;
            }

            int err = system(("xdg-open '" + authz_url + "' > /dev/null &").c_str());
            if(err != 0) {
                close(server);
                throw TransportError("Cannot start browser");
            }

            int conn = accept(server, nullptr, 0);
            if(conn < 0) {
                close(server);
                throw TransportError("Error on accept");
            }

            char buffer[1024];
            int count = read(conn, buffer, sizeof(buffer) -1);
            if (count < 0) {
                close(server);
                close(conn);
                throw TransportError("Error on read");
            }

            buffer[count] = '\0';

            std::string_view buf(buffer);

            auto pos = buf.find('\n');
            buf.remove_suffix(buf.size() - pos);

            if(buf.find("GET ") != 0) {
                close(conn);
                close(server);
                throw ProtocolError("Could not find GET");
            }
            buf.remove_prefix(4);
            pos = buf.rfind(" HTTP/");
            if(pos == std::string_view::npos) {
                close(conn);
                close(server);
                throw ProtocolError("Could not find HTTP/");
            }

            buf.remove_suffix(buf.size() - pos);

            std::string code;
            std::string ex_msg;  // exception message, empty -> no error
            if((pos = buf.find("state=")) != std::string_view::npos) {

                auto endpos = buf.find("&", pos);
                if(endpos == std::string_view::npos)
                    endpos = buf.size();

                if(random_state == buf.substr(pos + 6, endpos - pos - 6)) {

                    if((pos = buf.find("code=")) != std::string_view::npos) {

                        endpos = buf.find("&", pos);
                        if(endpos == std::string_view::npos)
                            endpos = buf.size();

                        code = std::string(buf.substr(pos + 5, endpos - pos - 5));
                    } else {
                        ex_msg = "Could not find 'code' parameter in reply";
                    }
                } else {
                    ex_msg = "State parameter is wrong";
                }

            } else {
                ex_msg = "Did not receive 'state' parameter";
            }

            send_text(conn, "HTTP/1.1 302 Redirect\r\nLocation: /result.html\r\n\r\n");

            do {
                close(conn);
                conn = accept(server, nullptr, 0);
                if(conn == -1) {
                    close(server);
                    throw TransportError("Error in accept");
                }

                int len = recv(conn, buffer, 1024, 0);
                buffer[len] = '\0';
                buf = buffer;
            } while(buf.find("/result.html") == std::string_view::npos);

            send_text(conn, "HTTP/1.1 200 OK\r\n\r\n<html><body>");
            if(ex_msg.empty()) {
                send_text(conn, success_msg.c_str());
            } else {
                send_text(conn, failure_msg.c_str());
                send_text(conn, "<p>More info: ");
                send_text(conn, ex_msg.c_str());
            }

            send_text(conn, "</body></html>");
            close(conn);
            close(server);

            if(!ex_msg.empty()) {
                throw TransportError(ex_msg);
            }

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());

            std::string post_data{"grant_type=authorization_code"};
            post_data += "&client_id=" + client_id;
            post_data += "&code=" + code;
            post_data += "&code_verifier=" + verifier;
            post_data += "&redirect_uri=http://localhost:" + std::to_string(port);
            if(!client_secret.empty()) {
                post_data += "&client_secret=" + client_secret;
            }

            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            std::string page;
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);

            check_curl_result(curl_easy_perform(handle));

            return page;
        }

        /// Acquire token from device code
        std::string
        get_token_from_device_code(const std::string& client_id,
                                   const std::string& client_secret,
                                   const std::string& scope,
                                   std::function<void (const std::string&, const std::string& url)> show_msg)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }
            check_metadata(handle);

            std::string verifier = get_code_verifier();
            std::string challenge = get_code_challenge(verifier);

            std::string nonce = get_code_verifier();

            std::string post_data;
            post_data += "client_id=" + client_id;
            if(!client_secret.empty()) {
                post_data += "&client_secret=" + client_secret;
            }
            post_data += "&code_challenge_method=S256&code_challenge=" + challenge + "&nonce=" + nonce;
            post_data += "&scope=openid" + (scope.empty() ? "" : "%20" + scope);

            curl_easy_setopt(handle, CURLOPT_URL, device_authorization_endpoint.c_str());
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            std::string page;
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);

            check_curl_result(curl_easy_perform(handle));

            nlohmann::json response;
            try {
                response = nlohmann::json::parse(page);
                std::string verification_uri = response["verification_uri"].get<std::string>();
                std::string verification_uri_complete = response["verification_uri_complete"].get<std::string>();

                unsigned int interval = response["interval"];
                time_t expires_at = time(nullptr) + response["expires_in"].get<unsigned int>();

                std::stringstream msg;
                msg  << "On your tablet, phone or computer, go to:\n\n"
                     << "    " << verification_uri
                     << "\n\nand enter the following code:\n\n"
                     << "    " << response["user_code"].get<std::string>() << "\n\n"
                     << "You may also open the following link directly and follow the instructions:\n\n"
                     << "    " <<  verification_uri_complete << std::endl;

                if(show_msg) {
                    show_msg(msg.str(), verification_uri_complete);
                } else {
                    std::cerr << msg.str();
                    std::unique_ptr<FILE, decltype(&pclose)> p(popen(("/usr/bin/qrencode -o - -t UTF8i " + verification_uri_complete + " 2> /dev/null").c_str(), "r"), pclose);
                    if(p) {
                        char buf[4096];
                        size_t len = fread(buf, 1, sizeof buf, p.get());
                        buf[len] = '\0';
                        std::cerr << buf;
                    }
                }

                std::cerr << "Waiting for signin (" << response["expires_in"] << " seconds max)...";

                post_data = "client_id=" + client_id;
                if(!client_secret.empty()) {
                    post_data += "&client_secret=" + client_secret;
                }

                post_data += "&grant_type=urn:ietf:params:oauth:grant-type:device_code";
                post_data += "&device_code=" + std::string(response["device_code"]);
                post_data += "&code_verifier=" + verifier;

                curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());
                curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());
                curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);
                curl_easy_setopt(handle, CURLOPT_WRITEDATA, &page);
                curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);

                long response_code;
                do {
                    sleep(interval);
                    std::cerr << '.';
                    page.clear();
                    check_curl_result(curl_easy_perform(handle));
                    curl_easy_getinfo(handle, CURLINFO_RESPONSE_CODE, &response_code);
                    response = nlohmann::json::parse(page);
                    if(response.contains("error") && response["error"] == "slow_down") {
                        interval += 5;
                        continue;
                    }
                } while ((response.contains("error") && response["error"] == "authorization_pending") && time(nullptr) < expires_at);

                std::cerr << std::endl;

                if(response.contains("error")) {
                    if(response.contains("error_description")) {
                        throw ProtocolError(response["error_description"]);
                    }
                    throw ProtocolError(response["error"]);
                }

                return page;

            } catch(SSOException& ex) {
                throw;
            } catch(...) {
                throw ProtocolError("Device authorization failed");
            }
        }

        std::string
        get_token_from_client_secret(const std::string& client_id,
                                     const std::string& client_secret,
                                     const std::string& scope)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }
            check_metadata(handle);

            std::string result;

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);

            std::string post_data{"grant_type=client_credentials"};
            post_data += "&client_id=" + client_id;
            post_data += "&client_secret=" + client_secret;
            post_data += "&scope=openid";
            if(!scope.empty()) {
                post_data += "%20" + scope;
            }
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            check_curl_result(curl_easy_perform(handle));

            return result;
        }

        ///
        /// Exchange your token against one for the target_client_id
        ///
        /// Has to be explicitly configured before use.
        std::string
        exchange_token(const std::string& client_id,
                       const std::string& client_secret,
                       const std::string& access_token,
                       const std::string& target_client_id)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }
            check_metadata(handle);

            std::string result;

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);

            std::string post_data{"grant_type=urn:ietf:params:oauth:grant-type:token-exchange"};
            post_data += "&client_id=" + client_id;
            post_data += "&client_secret=" + client_secret;
            post_data += "&audience=" + target_client_id;
            post_data += "&subject_token=" + access_token;
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            check_curl_result(curl_easy_perform(handle));

            return result;
        }

        ///
        /// Convert token to target client via CERN authorization API
        ///
        /// No prior configuration necessary, but 'sub' will be
        /// a 'service account' name, not the user login.
        ///
        std::string
        get_token_from_auth_api(const std::string& client_id,
                                const std::string& client_secret,
                                const std::string& target_client_id,
                                const std::string& scope)
        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }
            check_metadata(handle);

            std::string result;

            curl_easy_setopt(handle, CURLOPT_URL, (issuer + "/api-access/token").c_str());
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);

            std::string post_data{"grant_type=client_credentials"};
            post_data += "&client_id=" + client_id;
            post_data += "&client_secret=" + client_secret;
            post_data += "&audience=" + target_client_id;
            post_data += "&scope=openid";
            if(!scope.empty()) {
                post_data += "%20" + scope;
            }
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            check_curl_result(curl_easy_perform(handle));

            return result;
        }

        std::string get_token_from_password(const std::string& client_id,
                                            const std::string& username,
                                            const std::string& password,
                                            const std::string& client_secret,
                                            const std::string& scope)

        {
            std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> handler(curl_easy_init(), curl_easy_cleanup);
            CURL *handle = handler.get();
            if(handle == nullptr) {
                throw CURLError("Could not initialize CURL handle");
            }
            check_metadata(handle);

            std::string result;

            curl_easy_setopt(handle, CURLOPT_URL, token_endpoint.c_str());
            curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
            curl_easy_setopt(handle, CURLOPT_WRITEDATA, &result);
            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, receive_data);

            std::string post_data{"grant_type=password"};
            post_data += "&client_id=" + client_id;
            if(!client_secret.empty()) {
                post_data += "&client_secret=" + client_secret;
            }
            post_data += "&username=" + username;
            post_data += "&password=" + password;
            post_data += "&scope=openid";
            if(!scope.empty()) {
                post_data += "%20" + scope;
            }
            curl_easy_setopt(handle, CURLOPT_POSTFIELDS, post_data.c_str());

            check_curl_result(curl_easy_perform(handle));

            return result;
        }

        std::string
        get_token(const std::string& client_id,
                  const std::string& redirect_uri,
                  const std::string& cookie_file,
                  const std::string& scope,
                  const std::string& success_msg,
                  const std::string& failure_msg)
        {
            if(system("/usr/bin/klist -s") == 0) {
                try {
                    return get_token_from_kerberos(client_id, redirect_uri, cookie_file, scope);
                } catch(...) {
                }
            }

            if((getenv("DISPLAY") || getenv("WAYLAND_DISPLAY")) && redirect_uri == "http://localhost") {
                try {
                    return get_token_from_browser(client_id, scope, success_msg, failure_msg);
                } catch(...) {
                }
            }

            return get_token_from_device_code(client_id, scope);
        }

        bool
        parse_tokens(const std::string& input,
                     Tokens& output)
        {
            auto j = nlohmann::json::parse(input);
            output.clear();
            if(j.contains("access_token")) {
                output.access_token = j["access_token"];
                if(j.contains("refresh_token")) {
                    output.refresh_token = j["refresh_token"];
                }
                if(j.contains("id_token")) {
                    output.id_token = j["id_token"];
                }
                if(j.contains("scope")) {
                    output.scope = j["scope"];
                }
                auto now = time(nullptr);
                if(j.contains("expires_in")) {
                    output.access_token_expires_at = j["expires_in"];
                    output.access_token_expires_at += now;
                }
                if(j.contains("refresh_expires_in")) {
                    output.refresh_token_expires_at = j["refresh_expires_in"];
                    if(output.refresh_token_expires_at)
                        output.refresh_token_expires_at += now;
                }
                return true;
            }
            return false;
        }

        std::string
        set_metadata(const std::string& url)
        {
            auto old = meta_data_url;
            meta_data_url = url;
            // this triggers the retrieval on next use
            authorization_endpoint.clear();
            return old;
        }
    }
}
