/* SPDX-License-Identifier: Apache-2.0 */
// -*- this is c++ -*-
#ifndef DAQ_SSO_H_
#define DAQ_SSO_H_

#include <stdexcept>
#include <string>
#include <vector>
#include <functional>
#include <time.h>

namespace daq {

    namespace sso {

        ///
        /// Get SSO cookies or OIDC tokens from (CERN) SSO.
        ///

        /// Exceptions thrown
        class SSOException : public std::runtime_error
        {
        public:
            using runtime_error::runtime_error;
        };

        class CURLError : public SSOException
        {
        public:
            using SSOException::SSOException;
        };

        class ProtocolError : public SSOException
        {
        public:
            using SSOException::SSOException;
        };

        class TransportError : public SSOException
        {
        public:
            using SSOException::SSOException;
        };

        class TokenError : public SSOException
        {
        public:
            using SSOException::SSOException;
        };

        /// Helper struct to manage received tokens
        struct Tokens {
            Tokens() = default;
            explicit Tokens(const std::string& client, time_t before_expiration = 60)
                : client_id(client),
                  before_expiration(before_expiration) {}
            void
            clear();

            ///
            /// Returns access token if it is valid given the
            /// 'before_expiration' value, refreshes the tokens
            /// if necessary.
            std::string
            get_valid_access_token();

            std::string client_id;
            std::string access_token;
            std::string refresh_token;
            std::string id_token;
            std::string scope;
            time_t      access_token_expires_at = 0;
            time_t      refresh_token_expires_at = 0;
            time_t      before_expiration = 60;
        };

        /// Get a SSO protected web page.
        ///
        /// Use available kerberos ticket to acquire session cookies if they
        /// don't exist.
        ///
        /// Add headers to request.
        ///
        /// Saves the cookies in cookie_file if not empty.
        ///
        /// Returns the content of 'url' if successful, exception otherwise.
        std::string
        get(const std::string& url,
            const std::string& cookie_file = {},
            const std::vector<std::string>& headers = {});

        /// Post data to a SSO protected web page
        ///
        /// Use available kerberos ticket to acquire session cookies if they
        /// don't exist.
        ///
        /// Add headers to request.
        /// Add data to body to be posted. It is assumed to be properly
        /// encoded according to any headers you set.
        ///
        /// Saves the cookies in cookie_file if not empty.
        ///
        /// Returns the content of 'url' if successful, exception otherwise.
        std::string
        post(const std::string& url,
             const std::string& data,
             const std::string& cookie_file = {},
             const std::vector<std::string>& headers = {});

        ///
        /// Acquire a token from auth server using kerberos.
        ///
        /// Returns a JSON formatted string containing the access and refresh token
        ///
        std::string
        get_token_from_kerberos(const std::string& client_id,
                                const std::string& redirect_uri,
                                const std::string& cookie_file = {},
                                const std::string& scope = {});

        ///
        /// Refresh a token.
        ///
        /// Returns a JSON formatted string containing the access and refresh token
        ///
        std::string
        get_token_from_refresh_token(const std::string& client_id,
                                     const std::string& refresh_token,
                                     const std::string& client_secret = {});

        ///
        /// Acquire a token with the help of the user's browser.
        ///
        /// Returns a JSON formatted string containing the access and refresh token
        ///
        std::string
        get_token_from_browser(const std::string& client_id,
                               const std::string& client_secret = {},
                               const std::string& scope = {},
                               const std::string& success_msg = "<h2>Authentication succeeded ! You can close this window now</h2>",
                               const std::string& failure_msg = "<h2>Authentication failed !<h2>");

        ///
        /// Acquire token from device code
        ///
        /// Returns a JSON formatted string containing the access and refresh token
        ///
        std::string
        get_token_from_device_code(const std::string& client_id,
                                   const std::string& client_secret = {},
                                   const std::string& scope = {},
                                   std::function<void (const std::string&, const std::string& url)> show_msg = {});

        ///
        /// Acquire token from client credentials
        ///
        /// Note: the client has to be configured for this
        /// and the 'sub' of this token will be something like
        ///    service-account-${client-id}
        ///
        std::string
        get_token_from_client_secret(const std::string& client_id,
                                     const std::string& client_secret,
                                     const std::string& scope = {});

        ///
        /// Exchange your token against one for the target_client_id
        ///
        /// Has to be explicitly requested and configured before use.
        ///
        std::string
        exchange_token(const std::string& client_id,
                       const std::string& client_secret,
                       const std::string& access_token,
                       const std::string& target_client_id);

        ///
        /// Convert token to target client via CERN authorization API
        ///
        /// No prior configuration necessary, but 'sub' will be
        /// a 'service account' name, not the user login.
        ///
        std::string
        get_token_from_auth_api(const std::string& client_id,
                                const std::string& client_secret,
                                const std::string& target_client_id,
                                const std::string& scope = {});

        ///
        /// Try kerberos, browser, device method in turn
        ///
        std::string
        get_token(const std::string& client_id,
                  const std::string& redirect_uri,
                  const std::string& cookie_file = {},
                  const std::string& scope = {},
                  const std::string& success_msg = "<h2>Authentication succeeded ! You can close this window now</h2>",
                  const std::string& failure_msg = "<h2>Authentication failed !<h2>");

        [[deprecated("in OAuth2.1")]]
        std::string
        get_token_from_password(const std::string& client_id,
                                const std::string& username,
                                const std::string& password,
                                const std::string& client_secret = {},
                                const std::string& scope = {});

        ///
        /// Helper functions to parse returned result.
        ///
        /// Returns false if there is no access_token found
        /// Other return values may be empty (refresh_token)
        /// or zero (expiration times).
        ///
        /// The expiration times are absolute in seconds from
        /// the Unix epoch, so they can be easily checked with
        /// (note: refresh 30s before expiration):
        ///
        /// if(tokens.access_token_expires_at < time() + 30) {
        ///     result = get_token_from_refresh_token(tokens.client_id, tokens.refresh_token);
        ///     parse_tokens(result, tokens);
        /// }
        /// ...use tokens.access_token
        ///
        bool
        parse_tokens(const std::string& input,
                     Tokens& output);

        ///
        /// Use a different authentication server, URL points to metadata
        /// Returns previous metadata URL.
        ///
        /// Default is the CERN authentication server
        std::string
        set_metadata(const std::string& url);

    }
}

#endif // DAQ_SSO_H_
