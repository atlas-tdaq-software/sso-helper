#!/bin/bash
set -e
r=$(jq -r .refresh_token < /tmp/test-sso)
./get-sso-token refresh -c sso-helper-test -r $r
